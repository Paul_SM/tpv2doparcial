﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPV2doParcial
{
    public partial class Form1 : Form
    {
        int conteo;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = @"D:\Paul Meza\Documents\UNEDL2020B\unedl2020b\TPV2doParcial\segundoexamenparcial.txt";

            if (File.Exists(path))
            {
                tbxLocInv.Visible = true;
                btnIniciar.Enabled = false;
                btnIniciar.Visible = false;

                StreamReader stream = new StreamReader(path);
                string linea1 = null;
                int x = 0;
                while (!stream.EndOfStream)
                {
                    linea1 = stream.ReadLine();
                    if (++x == 10) break;
                }
                tbxCommand.Text = linea1;
                btnSiguiente.Visible = true;
                btnSiguiente.Enabled = true;
                //string text = File.ReadAllText(path);
                //tbxCommand.Text = text;
            }
            else
            {
                MessageBox.Show("Error Archivo Inexistente", "Error 3312",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (tbxLocInv.Text.Contains("/"))
            {
                tbxCommand.Text += tbxLocInv.Text;
                tbxLocInv.Visible = false;

                btnSiguiente.Enabled = false;
                btnSiguiente.Visible = false;

                btnSiguiente2.Enabled = true;
                btnSiguiente2.Visible = true;

                cbxInstalacion.Visible = true;

                string path = @"segundoexamenparcial.txt";
                StreamReader stream = new StreamReader(path);

                string linea2 = null;
                int y = 0;
                while (!stream.EndOfStream)
                {
                    linea2 = stream.ReadLine();
                    if (++y == 22) break;
                }
                tbxCommand.Text += "\r\n" + linea2;
            }
            else
            {
                MessageBox.Show("Error de Ubicación", "Error 3312",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSiguiente2_Click(object sender, EventArgs e)
        {
            tbxLocBas.Visible = true;

            btnSiguiente2.Enabled = false;
            btnSiguiente2.Visible = false;

            btnSiguiente3.Enabled = true;
            btnSiguiente3.Visible = true;

            cbxInstalacion.Visible = false;

            tbxCommand.Text += cbxInstalacion.Text;

            string path = @"segundoexamenparcial.txt";
            StreamReader stream = new StreamReader(path);

            string linea3 = null;
            int z = 0;
            while (!stream.EndOfStream)
            {
                linea3 = stream.ReadLine();
                if (++z == 29) break;
            }
            tbxCommand.Text += "\r\n" + linea3;
        }

        private void btnSiguiente3_Click(object sender, EventArgs e)
        {
            if (tbxLocBas.Text.Contains("/"))
            {
                tbxCommand.Text += tbxLocBas.Text;
                tbxLocBas.Visible = false;

                tbxContra.Visible = true;

                btnSiguiente3.Enabled = false;
                btnSiguiente3.Visible = false;

                btnSiguiente4.Enabled = true;
                btnSiguiente4.Visible = true;

                string path = @"segundoexamenparcial.txt";
                StreamReader stream = new StreamReader(path);

                string linea4 = null;
                int a = 0;
                while (!stream.EndOfStream)
                {
                    linea4 = stream.ReadLine();
                    if (++a == 48) break;
                }
                tbxCommand.Text += "\r\n" + linea4;
            }
            else
            {
                MessageBox.Show("Error de Ubicación", "Error 3312",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSiguiente4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxContra.Text))
            {
                tbxCommand.Text += "NULL";
                tbxContra.Visible = false;

                btnSiguiente4.Enabled = false;
                btnSiguiente4.Visible = false;

                btnSiguiente5.Enabled = true;
                btnSiguiente5.Visible = true;

                cbxRedundancia.Visible = true;

                string path = @"segundoexamenparcial.txt";
                StreamReader stream = new StreamReader(path);

                string linea5 = null;
                int b = 0;
                while (!stream.EndOfStream)
                {
                    linea5 = stream.ReadLine();
                    if (++b == 59) break;
                }
                tbxCommand.Text += "\r\n" + linea5;
            }
            else
            {
                tbxCommand.Text += tbxContra.Text;
                tbxContra.Visible = false;

                btnSiguiente4.Enabled = false;
                btnSiguiente4.Visible = false;
                btnSiguiente5.Enabled = true;
                btnSiguiente5.Visible = true;

                cbxRedundancia.Visible = true;

                string path = @"segundoexamenparcial.txt";
                StreamReader stream = new StreamReader(path);

                string linea5 = null;
                int b = 0;
                while (!stream.EndOfStream)
                {
                    linea5 = stream.ReadLine();
                    if (++b == 59) break;
                }
                tbxCommand.Text += "\r\n" + linea5;
            }
        }

        private void btnSiguiente5_Click(object sender, EventArgs e)
        {
            tbxCommand.Text += cbxRedundancia.Text;
            cbxRedundancia.Visible = false;

            btnSiguiente5.Enabled = false;
            btnSiguiente5.Visible = false;
            btnSiguiente6.Enabled = true;
            btnSiguiente6.Visible = true;

            tbxPort.Visible = true;

            string path = @"segundoexamenparcial.txt";
            StreamReader stream = new StreamReader(path);

            string linea6 = null;
            int c = 0;
            while (!stream.EndOfStream)
            {
                linea6 = stream.ReadLine();
                if (++c == 36) break;
            }
            tbxCommand.Text += "\r\n" + linea6;
        }

        private void btnSiguiente6_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxPort.Text))
            {
                MessageBox.Show("Port Vacio", "Error 3312",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                tbxCommand.Text += tbxPort.Text;
                tbxPort.Visible = false;

                btnSiguiente6.Enabled = false;
                btnSiguiente6.Visible = false;
                btnFinalizar.Enabled = true;
                btnFinalizar.Visible = true;

                tbxIP.Visible = true;

                string path = @"segundoexamenparcial.txt";
                StreamReader stream = new StreamReader(path);

                string linea7 = null;
                int d = 0;
                while (!stream.EndOfStream)
                {
                    linea7 = stream.ReadLine();
                    if (++d == 42) break;
                }
                tbxCommand.Text += "\r\n" + linea7;
            } 
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxIP.Text))
            {
                MessageBox.Show("IP Vacia", "Error 3312",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                tbxCommand.Text += tbxIP.Text;
                timer1.Start();

                StreamWriter escrito = File.CreateText(@"Inventario.inv");

                String contenido = tbxCommand.Text;
                escrito.Write(contenido.ToString());
                escrito.Flush();
                escrito.Close();

                MessageBox.Show("Terminando La Instalación", "Espere...",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            conteo++;
            Cursor.Current = Cursors.WaitCursor;
            if (conteo == 5)
            {
                Application.ExitThread();
            }
        }

        private void tbxLocInv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;
                this.tbxLocInv.Focus();
            }
        }

        private void tbxLocBas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;
                this.tbxLocBas.Focus();
            }
        }

        private void tbxContra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;
                this.tbxLocBas.Focus();
            }
        }

        private void tbxLocInv_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxPort_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void tbxIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}