﻿namespace TPV2doParcial
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbxCommand = new System.Windows.Forms.TextBox();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.cbxInstalacion = new System.Windows.Forms.ComboBox();
            this.btnSiguiente2 = new System.Windows.Forms.Button();
            this.btnSiguiente3 = new System.Windows.Forms.Button();
            this.btnSiguiente4 = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.cbxRedundancia = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tbxLocInv = new System.Windows.Forms.TextBox();
            this.tbxLocBas = new System.Windows.Forms.TextBox();
            this.tbxContra = new System.Windows.Forms.TextBox();
            this.btnSiguiente5 = new System.Windows.Forms.Button();
            this.btnSiguiente6 = new System.Windows.Forms.Button();
            this.tbxPort = new System.Windows.Forms.TextBox();
            this.tbxIP = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbxCommand
            // 
            this.tbxCommand.BackColor = System.Drawing.SystemColors.MenuText;
            this.tbxCommand.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCommand.ForeColor = System.Drawing.Color.Lime;
            this.tbxCommand.Location = new System.Drawing.Point(12, 12);
            this.tbxCommand.Multiline = true;
            this.tbxCommand.Name = "tbxCommand";
            this.tbxCommand.ReadOnly = true;
            this.tbxCommand.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxCommand.Size = new System.Drawing.Size(240, 297);
            this.tbxCommand.TabIndex = 0;
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(258, 12);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(81, 35);
            this.btnIniciar.TabIndex = 1;
            this.btnIniciar.Text = "Iniciar Instalación";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(258, 274);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(81, 35);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar Instalación";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Enabled = false;
            this.btnSiguiente.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente.TabIndex = 3;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Visible = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // cbxInstalacion
            // 
            this.cbxInstalacion.FormattingEnabled = true;
            this.cbxInstalacion.Items.AddRange(new object[] {
            "FULL",
            "PART",
            "MIN"});
            this.cbxInstalacion.Location = new System.Drawing.Point(258, 159);
            this.cbxInstalacion.Name = "cbxInstalacion";
            this.cbxInstalacion.Size = new System.Drawing.Size(81, 21);
            this.cbxInstalacion.TabIndex = 4;
            this.cbxInstalacion.Text = "FULL";
            this.cbxInstalacion.Visible = false;
            // 
            // btnSiguiente2
            // 
            this.btnSiguiente2.Enabled = false;
            this.btnSiguiente2.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente2.Name = "btnSiguiente2";
            this.btnSiguiente2.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente2.TabIndex = 5;
            this.btnSiguiente2.Text = "Siguiente";
            this.btnSiguiente2.UseVisualStyleBackColor = true;
            this.btnSiguiente2.Visible = false;
            this.btnSiguiente2.Click += new System.EventHandler(this.btnSiguiente2_Click);
            // 
            // btnSiguiente3
            // 
            this.btnSiguiente3.Enabled = false;
            this.btnSiguiente3.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente3.Name = "btnSiguiente3";
            this.btnSiguiente3.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente3.TabIndex = 6;
            this.btnSiguiente3.Text = "Siguiente";
            this.btnSiguiente3.UseVisualStyleBackColor = true;
            this.btnSiguiente3.Visible = false;
            this.btnSiguiente3.Click += new System.EventHandler(this.btnSiguiente3_Click);
            // 
            // btnSiguiente4
            // 
            this.btnSiguiente4.Enabled = false;
            this.btnSiguiente4.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente4.Name = "btnSiguiente4";
            this.btnSiguiente4.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente4.TabIndex = 7;
            this.btnSiguiente4.Text = "Siguiente";
            this.btnSiguiente4.UseVisualStyleBackColor = true;
            this.btnSiguiente4.Visible = false;
            this.btnSiguiente4.Click += new System.EventHandler(this.btnSiguiente4_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Enabled = false;
            this.btnFinalizar.Location = new System.Drawing.Point(258, 233);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(81, 35);
            this.btnFinalizar.TabIndex = 8;
            this.btnFinalizar.Text = "Finalizar Instalación";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Visible = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // cbxRedundancia
            // 
            this.cbxRedundancia.FormattingEnabled = true;
            this.cbxRedundancia.Items.AddRange(new object[] {
            "NORMAL",
            "HIGH",
            "LOW"});
            this.cbxRedundancia.Location = new System.Drawing.Point(258, 159);
            this.cbxRedundancia.Name = "cbxRedundancia";
            this.cbxRedundancia.Size = new System.Drawing.Size(81, 21);
            this.cbxRedundancia.TabIndex = 9;
            this.cbxRedundancia.Text = "NORMAL";
            this.cbxRedundancia.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tbxLocInv
            // 
            this.tbxLocInv.Location = new System.Drawing.Point(258, 160);
            this.tbxLocInv.Name = "tbxLocInv";
            this.tbxLocInv.Size = new System.Drawing.Size(81, 20);
            this.tbxLocInv.TabIndex = 10;
            this.tbxLocInv.Visible = false;
            this.tbxLocInv.TextChanged += new System.EventHandler(this.tbxLocInv_TextChanged);
            this.tbxLocInv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxLocInv_KeyDown);
            // 
            // tbxLocBas
            // 
            this.tbxLocBas.Location = new System.Drawing.Point(258, 160);
            this.tbxLocBas.Name = "tbxLocBas";
            this.tbxLocBas.Size = new System.Drawing.Size(81, 20);
            this.tbxLocBas.TabIndex = 11;
            this.tbxLocBas.Visible = false;
            this.tbxLocBas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxLocBas_KeyDown);
            // 
            // tbxContra
            // 
            this.tbxContra.Location = new System.Drawing.Point(258, 160);
            this.tbxContra.MaxLength = 16;
            this.tbxContra.Name = "tbxContra";
            this.tbxContra.Size = new System.Drawing.Size(81, 20);
            this.tbxContra.TabIndex = 12;
            this.tbxContra.Visible = false;
            this.tbxContra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxContra_KeyDown);
            // 
            // btnSiguiente5
            // 
            this.btnSiguiente5.Enabled = false;
            this.btnSiguiente5.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente5.Name = "btnSiguiente5";
            this.btnSiguiente5.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente5.TabIndex = 13;
            this.btnSiguiente5.Text = "Siguiente";
            this.btnSiguiente5.UseVisualStyleBackColor = true;
            this.btnSiguiente5.Visible = false;
            this.btnSiguiente5.Click += new System.EventHandler(this.btnSiguiente5_Click);
            // 
            // btnSiguiente6
            // 
            this.btnSiguiente6.Enabled = false;
            this.btnSiguiente6.Location = new System.Drawing.Point(258, 53);
            this.btnSiguiente6.Name = "btnSiguiente6";
            this.btnSiguiente6.Size = new System.Drawing.Size(81, 35);
            this.btnSiguiente6.TabIndex = 14;
            this.btnSiguiente6.Text = "Siguiente";
            this.btnSiguiente6.UseVisualStyleBackColor = true;
            this.btnSiguiente6.Visible = false;
            this.btnSiguiente6.Click += new System.EventHandler(this.btnSiguiente6_Click);
            // 
            // tbxPort
            // 
            this.tbxPort.Location = new System.Drawing.Point(258, 160);
            this.tbxPort.MaxLength = 5;
            this.tbxPort.Name = "tbxPort";
            this.tbxPort.Size = new System.Drawing.Size(81, 20);
            this.tbxPort.TabIndex = 15;
            this.tbxPort.Visible = false;
            this.tbxPort.TextChanged += new System.EventHandler(this.tbxPort_TextChanged);
            this.tbxPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxPort_KeyPress);
            // 
            // tbxIP
            // 
            this.tbxIP.Location = new System.Drawing.Point(258, 160);
            this.tbxIP.MaxLength = 13;
            this.tbxIP.Name = "tbxIP";
            this.tbxIP.Size = new System.Drawing.Size(81, 20);
            this.tbxIP.TabIndex = 3;
            this.tbxIP.Visible = false;
            this.tbxIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxIP_KeyPress);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(346, 321);
            this.Controls.Add(this.tbxIP);
            this.Controls.Add(this.tbxPort);
            this.Controls.Add(this.btnSiguiente6);
            this.Controls.Add(this.btnSiguiente5);
            this.Controls.Add(this.tbxContra);
            this.Controls.Add(this.tbxLocBas);
            this.Controls.Add(this.tbxLocInv);
            this.Controls.Add(this.cbxRedundancia);
            this.Controls.Add(this.btnFinalizar);
            this.Controls.Add(this.btnSiguiente4);
            this.Controls.Add(this.btnSiguiente3);
            this.Controls.Add(this.btnSiguiente2);
            this.Controls.Add(this.cbxInstalacion);
            this.Controls.Add(this.btnSiguiente);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnIniciar);
            this.Controls.Add(this.tbxCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "⛏ ¦  Instalador";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxCommand;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.ComboBox cbxInstalacion;
        private System.Windows.Forms.Button btnSiguiente2;
        private System.Windows.Forms.Button btnSiguiente3;
        private System.Windows.Forms.Button btnSiguiente4;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.ComboBox cbxRedundancia;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox tbxLocInv;
        private System.Windows.Forms.TextBox tbxLocBas;
        private System.Windows.Forms.TextBox tbxContra;
        private System.Windows.Forms.Button btnSiguiente5;
        private System.Windows.Forms.Button btnSiguiente6;
        private System.Windows.Forms.TextBox tbxPort;
        private System.Windows.Forms.TextBox tbxIP;
    }
}

